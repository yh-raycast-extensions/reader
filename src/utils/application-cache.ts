import { Cache } from "@raycast/api";

export interface CacheEntry<T> {
  value: T;
  storedAt: Date;
}

export interface Subscription {
  unsubscribe(): void;
}

export abstract class ApplicationCache {
  constructor(private cache: Cache) {}

  protected set<T>(key: string, value: T) {
    const entry: CacheEntry<T> = {
      value,
      storedAt: new Date(),
    };
    this.cache.set(key, JSON.stringify(entry));
  }

  protected get<T>(key: string): CacheEntry<T> | undefined {
    const cachedValue = this.cache.get(key);
    if (cachedValue == null) {
      return undefined;
    }

    try {
      const entry = JSON.parse(cachedValue);
      if (typeof entry === "object" && entry.hasOwnProperty("value") && entry.hasOwnProperty("storedAt")) {
        return {
          value: entry.value as T,
          storedAt: new Date(entry.storedAt),
        };
      }
    } catch {
      return undefined;
    }
  }

  subscribe(subscriber: () => void): Subscription {
    const unsubscribe = this.cache.subscribe(subscriber);

    return {
      unsubscribe,
    };
  }
}
