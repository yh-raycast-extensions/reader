import { List, ActionPanel, Action, Icon } from "@raycast/api";
import { Story, CachedStoryState } from "./hacker-news/types";
import { useTopStories } from "./hacker-news/use-top-stories";

interface StoryItemProps {
  story: CachedStoryState;
}

function StoryItem({ story }: StoryItemProps) {
  return story.state === "UNAVAILABLE" ? (
    <List.Item title="Loading..." />
  ) : (
    <List.Item
      title={story.value.title}
      actions={<StoryActionPanel story={story.value} />}
      accessories={[{ text: `${story.value.comments}`, icon: Icon.SpeechBubbleActive }]}
    />
  );
}

interface StoryActionPanelProps {
  story: Story;
}

function StoryActionPanel({ story }: StoryActionPanelProps) {
  return (
    <ActionPanel>
      <Action.OpenInBrowser title="View Comments" url={`https://news.ycombinator.com/item?id=${story.id}`} />
      {story.url && <Action.OpenInBrowser url={story.url} />}
    </ActionPanel>
  );
}

export default function Command() {
  const { feed, isLoading } = useTopStories();

  if (feed.state === "UNAVAILABLE") {
    return <List navigationTitle="Hacker News" isLoading={isLoading} />;
  }

  return (
    <List navigationTitle="Hacker News" isLoading={isLoading}>
      {feed.value.map((story, idx) => (
        <StoryItem key={idx} story={story} />
      ))}
    </List>
  );
}
