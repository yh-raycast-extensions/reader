import { Cache } from "@raycast/api";
import { ApplicationCache, CacheEntry } from "../utils/application-cache";
import { Story } from "./types";

export class HackerNewsCache extends ApplicationCache {
  constructor() {
    super(new Cache({ namespace: "hacker-news" }));
  }

  setTopStoryIDs(ids: ReadonlyArray<string>) {
    super.set("top-story-ids", ids);
  }

  getTopStoryIDs(): CacheEntry<Array<string>> | undefined {
    return super.get("top-story-ids");
  }

  setStory(id: string, story: Story) {
    super.set(`story-${id}`, story);
  }

  getStory(id: string): CacheEntry<Story> | undefined {
    return super.get(`story-${id}`);
  }
}
