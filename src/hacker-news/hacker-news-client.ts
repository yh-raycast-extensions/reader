import fetch from "cross-fetch";
import { Story } from "./types";

export class HackerNewsClient {
  async fetchTopStoryIDs(): Promise<Array<string>> {
    const response = await fetch("https://hacker-news.firebaseio.com/v0/topstories.json");
    const json = (await response.json()) as ReadonlyArray<number>;
    return json.map((id) => `${id}`);
  }

  async fetchStory(id: string): Promise<Story> {
    const response = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`);
    const json = (await response.json()) as any;
    return {
      id: `${json.id}`,
      title: json.title,
      url: json.url,
      points: json.score,
      comments: json.descendants,
    };
  }
}
