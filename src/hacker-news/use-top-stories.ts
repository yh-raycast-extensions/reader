import { getPreferenceValues } from "@raycast/api";
import { useCallback, useState, useEffect } from "react";
import { useIsMounted } from "../utils/use-is-mounted";
import { CachedStoryState, CachedFeedState } from "./types";
import { HackerNewsCache } from "./hacker-news-cache";
import { HackerNewsClient } from "./hacker-news-client";

const client = new HackerNewsClient();
const cache = new HackerNewsCache();

interface Preferences {
  itemsToShow: string;
}

interface Output {
  feed: CachedFeedState;
  isLoading: boolean;
}

export function useTopStories(): Output {
  const isMounted = useIsMounted();

  // TODO: Should we make this a number, so that if there are multiple attempts, we won't prematurely set it to false
  const [loadingAttempts, setLoadingAttempts] = useState(0);
  const [feed, setFeed] = useState<CachedFeedState>(getCachedFeedState);

  const refreshFeed = useCallback(async () => {
    setLoadingAttempts((x) => x + 1);
    const topStoryIDs = await client.fetchTopStoryIDs();
    cache.setTopStoryIDs(topStoryIDs);

    const stories = await Promise.all(
      topStoryIDs.map(async (id) => {
        const story = await client.fetchStory(id);
        cache.setStory(id, story);
        return story;
      }),
    );

    if (isMounted.current) {
      setLoadingAttempts((x) => x - 1);
    }

    return stories;
  }, []);

  useEffect(() => {
    const subscription = cache.subscribe(() => {
      setFeed(getCachedFeedState());
    });

    refreshFeed();

    return () => subscription.unsubscribe();
  }, []);

  return {
    feed,
    isLoading: loadingAttempts === 0,
  };
}

function getCachedFeedState(): CachedFeedState {
  const preferences = getPreferenceValues<Preferences>();
  const items = parseInt(preferences.itemsToShow);

  const cachedTopStoryIDs = cache.getTopStoryIDs();
  if (cachedTopStoryIDs == null) {
    return { state: "UNAVAILABLE" };
  }

  const cachedStoryStates: ReadonlyArray<CachedStoryState> = cachedTopStoryIDs.value.slice(0, items).map((id) => {
    const cachedStory = cache.getStory(id);
    return cachedStory == null ? { state: "UNAVAILABLE" } : { state: "AVAILABLE", value: cachedStory.value };
  });

  return { state: "AVAILABLE", value: cachedStoryStates };
}
