export interface Story {
  id: string;
  title: string;
  url: string | undefined;
  points: number;
  comments: number;
}

export type CachedState<T> = { state: "UNAVAILABLE" } | { state: "AVAILABLE"; value: T };
export type CachedStoryState = CachedState<Story>;
export type CachedFeedState = CachedState<ReadonlyArray<CachedStoryState>>;
